<?php 
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php"); } */ include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106247\Dates\Birthday;
	$obj = new Birthday;
?>
<!DOCTYPE html>
<html>
<head><meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(20) CRUD</title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create","store","edit","update","delete"];
							$getid= 5;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php?id={$key}'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>This is Delete Page.</h1>
					<div class="formArea">
						<p>
						<?php
							$obj->delete(); 
						?>
						</p>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>