<?php 
	ini_set("display_errors","off");
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php"); }
	*/
	include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106247\Library\Books;
	$obj = new Books;
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(20) CRUD</title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 1;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>This is Create Page.</h1>
					<div class="formArea">
					<form class="form-horizontal" method="post" action="store.php">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Your Book :</label>
							<div class="col-sm-6">
							  <input type="text" name="title" class="form-control" placeholder="Book">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">Author Name:</label>
							<div class="col-sm-6">
							  <input type="text" name="author" class="form-control" placeholder="Author">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
							  <button type="submit" name="submit" class="btn btn-success">Add</button>
							</div>
						  </div>
					</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

