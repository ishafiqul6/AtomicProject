<?php 
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php");
	}
	*/
	include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106247\Library\Books;
	use App\Bitm\SEIP106247\Utility\Utility;
	if(isset($_POST["submit"])){
		$obj = new Books($_POST);
		$obj->store();
	}else{
		Utility::redirect("index.php");
	}
?>