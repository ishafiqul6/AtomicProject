<?php 
	ini_set("display_errors","off");
	/* function __autoload($className){
		$file = str_replace("\\","/", $className);
		require_once("../../../".$file.".php");
	}
	*/
	include_once("../../../vendor/autoload.php");
	use App\Bitm\SEIP106247\Library\Books;
	$obj = new Books;
	$results = $obj->index();
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(20) CRUD</title>
	<link rel="stylesheet" href="../../../resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<a href="../../../" class="back">&larr; Back</a>
					<hr>
					<ul class="nav">
						<?php 
							$files = ["index","create"];
							$getid= 0;
							foreach($files as $key => $file){
								$name = ucfirst($file);
								$output ="<li><a class='";
									if($key == $getid){$output .= "active";}
								$output .= "' href='{$file}.php'>{$name}</a></li>";
								echo $output;
							}
						?>
					</ul>
					<hr>
					<h1>This is List View Page.</h1>
					<div class="listArea">
						<div class="tableInfo">
							<div class="row">
								<div class="col-xs-4 col-sm-3 col-md-2">
									<select class="items" name="items">
										<option value="10">10</option>
										<option value="20">20</option>
										<option value="30">30</option>
										<option value="40">40</option>
									</select>
								</div>
								<div class="col-xs-8 col-sm-9 col-md-10"></div>
							</div>
						</div>
						<table class="table table-bordered">
							<thead class="text-center">
								<tr>
									<th>ID</th>
									<th>Books</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody class="text-center">
								<?php 
									$slNo = 1;
									foreach($results as $result){
								?>
								<tr>
									<td><?php echo $slNo; ?></td>
									<td>
<a href="single.php?id=<?php echo $result->id; ?>"><?php echo $result->title; ?></a>
									</td>
									<td>
<a href="edit.php?id=<?php echo $result->id; ?>" class="list-btn"><img src="../../../resource/images/edit.png"></a>
<a href="delete.php?id=<?php echo $result->id; ?>" class="list-btn"><img src="../../../resource/images/delete.png"></a>
<a href="remove.php?id=<?php echo $result->id; ?>" class="list-btn"><img src="../../../resource/images/remove.png"></a>
									</td>
								</tr>
								<?php 
									++$slNo;
									}
								?>
							</tbody>
						</table>
					</div>
					<div class="listBox">
						<ul class="listLink">
							<li><a href="p1.html">1</a>&nbsp;&nbsp;&gt; </li>
							<li><a href="p2.html">2</a>&nbsp;&nbsp;&gt; </li>
							<li><a href="p3.html">3</a></li>
							<li><a class="next" href="p3.html"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>