<!DOCTYPE html>
<html>
<head><meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Class(20) CRUD</title>
	<link rel="stylesheet" href="resource/css/bootstrap.min.css">
	<link rel="stylesheet" href="resource/css/style.css">
</head>
<body>
	<div class="wrapper">
		<div class="container bg">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h1>PHP Batch-11</h1>
					<h2>Md. SHAFIQUL ISLAM</h2>
					<h3>ID: 106247</h3>
					<hr>
					<h4>ALL ATOMIC PROJECT LIST</h4>
					<?php $folders = ["Birthdays","Library","Subscription","Tools"];?>
					<ul class="mainNav">
						<?php
							foreach($folders as $key => $folder){
								echo"<li><a href='views/SEIP106247/{$folder}/'>{$folder}</a></li>";
							}
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>