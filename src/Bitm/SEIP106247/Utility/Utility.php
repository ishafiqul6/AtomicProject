<?php 
	namespace App\Bitm\SEIP106247\Utility;
	class Utility{
		public static function dump($var){
			$output  = "<pre>";
			$output .= var_dump($var);
			$output .= "</pre>";
			return $output;
		}
		public static function dumpDie($var){
			$output  = self::dump($var);
			$output .= die();
			return $output;
		}
		public static function redirect($url="../../../../index.php"){
			header("Location:".$url);
			exit();
		}
		public static function changeFormat($date=false,$format="d/m/Y"){
			$dates = str_replace("/","-",$date);
			$dates = date_create($dates);
			$formats = date_format($dates, $format);
			return $formats;
		}
	}
?>